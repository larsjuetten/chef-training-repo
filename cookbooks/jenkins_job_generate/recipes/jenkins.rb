#
# Cookbook Name:: jenkins_job_generate
# Recipe:: jenkins
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'jenkins::master'

package 'git'

node['jenkins_job_generate']['plugins'].each do |plugin|
  plugin, version = plugin.split('=')
  jenkins_plugin plugin do
    version version if version
    notifies :execute, 'jenkins_command[restart]', :immediately
  end
end

jenkins_command 'restart' do
  action :nothing
end
