#
# Cookbook Name:: jenkins_job_generate
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

%w(mlocate bash-completion).each do |pkg|
  package pkg
end

include_recipe 'java::default'
include_recipe 'jenkins_job_generate::jenkins'
include_recipe 'jenkins_job_generate::setup'
include_recipe 'jenkins_job_generate::jobs'
