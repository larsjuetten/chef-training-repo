#
# Cookbook Name:: jenkins_job_generate
# Recipe:: setup
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

chef_gem 'gitlabuddy' do
  compile_time true
  version '0.0.5'
end
