describe file('/etc/centos-release') do
  its('content') { should match(/^CentOS Linux release 7\.1/) }
end

describe command('java -version') do
  its(:stderr) { should match(/^java version "1\.8\.0_\d+"$/) }
  its(:exit_status) { should eq 0 }
 end

describe service('jenkins') do
  # it { should be_enabled }
  it { should be_installed }
  it { should be_running }
end

describe command('systemctl is-enabled jenkins.service')  do
  its(:stdout) { should matc(/^enabled$/)}
end
