default['jenkins']['master']['version'] = '1.658-1.1'
default['jenkins_job_generate']['plugins'] = %w(
  git=2.5.0
  git-client
  scm-api
  structs
  job-dsl=1.47
  promoted-builds
  greenballs
)
