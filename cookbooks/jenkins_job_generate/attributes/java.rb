default['java']['jdk_version'] = '8'
default['java']['install_flavour'] = 'oracle'
default['java']['oracle']['accept_oracle_download_terms'] = true
default['java']['accept_license_agreement'] = true
